# Gnucash docker

This repository contains a Dockerfile with the latest Gnucash version.

## Build and Run

 * Build the image: `docker build -t gnucash .`
 * Run: `docker run --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v <path-to-gnucash-dir>:/home/gnucash/gnucash gnucash`


## Run

The Docker image is available on [`quay.io`](quay.io/flavius/gnucash)

 * Run: `docker run --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v <path-to-gnucash-dir>:/home/gnucash/gnucash quay.io/flavius/gnucash`
