FROM debian:sid
# debian sid has the latest gnucash version (version 3.4)

# install gnucash
RUN apt update &&\
    apt install -yq gnucash dbus-x11

# Replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/gnucash && \
    echo "gnucash:x:${uid}:${gid}:Gnucash,,,:/home/gnucash:/bin/bash" >> /etc/passwd && \
    echo "gnucash:x:${uid}:" >> /etc/group && \
    chown ${uid}:${gid} -R /home/gnucash

USER gnucash
ENV HOME /home/gnucash
ENV DISPLAY :0

ENTRYPOINT ["gnucash", "--logto", "stderr"]
# docker run --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v <path-to-gnucash-dir>:/home/gnucash/gnucash gnucash:latest