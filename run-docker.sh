#!/bin/bash

# try to start the container with gnucash or create a new one
docker start gnucash || \
    docker run --name gnucash -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v $1:/home/gnucash/gnucash gnucash:latest
